#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] scribbleed
#
# Original Attribution of voronoi2svg.py:
#   Copyright (C) 2011 Vincent Nivoliers and contributors
#
#   Contributors
#      ~suv, <suv-sf@users.sf.net>
#   - Voronoi Diagram algorithm and C code by Steven Fortune, 1987, http://ect.bell-labs.com/who/sjf/
#   - Python translation to file voronoi.py by Bill Simons, 2005, http://www.oxfish.com/
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
Create a low poly diagram using Delaunay triangulation,
with color from underlying drawing
"""

import inkex
from inkex import Group, Path, PathElement, Vector2d as Point
from inkex.command import inkscape

from PIL import Image
import numpy as np
import os
import tempfile

# requires voronoi.py extension which comes with standard Inkscape installation
import voronoi

class LowPolyTrace(inkex.EffectExtension):
    """Extension to create low poly triangles"""

    def add_arguments(self, pars):
        pars.add_argument("--tab")
        pars.add_argument(
            "--fill",
            dest      = "fill",
            type      = inkex.Boolean,
            default   = True,
            help      = "Fill triangles with underlying color",
        )
        pars.add_argument(
            "--range",
            dest      = "range",
            type      = int,
            default   = 5,
            help      = "Range in pixels to ignore nearby points"
        )

    def effect(self):
        paths = self.svg.selection.filter(PathElement)
        if len(paths) == 0:
            raise inkex.AbortExtension("No paths in selection")

        ppdu  = self.svg.unit_to_viewport("1")  # pixel per dimensionless unit
        fill  = self.options.fill
        range = self.options.range / ppdu

        # Collect node points, remove duplicate/nearby points
        # Derived from code in voronoi2svg.py
        parent_group  = paths.first().getparent()
        gtransform    = parent_group.composed_transform()
        seeds         = []
        unique        = []
        p             = 0
        for path in paths:
            eps = list(path.path.end_points)
            ptransform = -gtransform @ path.composed_transform()
            for ep in eps:
                x, y = ep.x, ep.y
                if ptransform:
                    [x, y] = ptransform.apply_to_point([x, y])
                ux, uy = x - x % range, y - y % range
                if (ux, uy) not in unique:
                    unique.append((ux, uy))
                    seeds.append(Point(x, y))
                    p += 1
        if p < 3 :
            raise inkex.AbortExtension("At least 3 distinct points required")

        # Code to create array for color reference
        # by first creating a temporary non-transparent png file
        # with background of pagecolor
        if fill:
            pagecolor       = self.svg.namedview.get("pagecolor", "#FFFFFF")  # default white if none
            bboxes          = [path.bounding_box(gtransform) for path in paths]
            bb              = sum(bboxes, None)
            (width, height) = bb.size
            (top, left)     = bb.minimum
            (bottom, right) = bb.maximum
            area            = "%f:%f:%f:%f" % (top*ppdu, left*ppdu, bottom*ppdu, right*ppdu)
            # Bounding box values are in SVG dimensionless units
            # Area export must be in pixel values to get the correct location of the selected box
            # (width, height) are not converted so that SVG points, after offset, correspond 1 to 1 in exported image
            # Effectively, the image is downscaled, but this would give an 'average' color value
            
            (handle, filename) = tempfile.mkstemp(".png")
            svg_file = self.options.input_file
            kwargs   = {
                        "export-filename"       : filename,
                        "export-type"           : "png",
                        "export-png-color-mode" : "RGB_8",
                        "export-area"           : area,
                        "export-width"          : str(int(width  + .5)),
                        "export-height"         : str(int(height + .5)),
                        "export-background"     : pagecolor
                       }
            inkscape(svg_file, **kwargs)  # call Inkscape to export area to png file
            image = Image.open(filename)
            image_array = np.array(image)
            image.close()
            os.close(handle)
            os.remove(filename)
        # Temporary directory and png file are deleted here but array is kept for reference
        # To get color of (x,y) within the exported area, use:
        #     (r, g, b) = image_array[int(y-left)][int(x-top)]
        # Numpy coordinates are reversed, index must be integer
        # (x,y) must be in absolute SVG units i.e. transformed if specified
        # (top, left) is already transformed above

        # Creation of group and default style
        # Derived from code in voronoi2svg.py
        group = parent_group.add(Group())
        group.set("inkscape:label", "LowPoly")

        strokewidth = str(1/ppdu) # 1 pixel

        facestyle = {
                     "fill"            : "none",
                     "stroke"          : "#000000",
                     "stroke-width"    : strokewidth,
                     "stroke-linecap"  : "round",
                     "stroke-linejoin" : "round"
                    } # default if no fill

        # Delaunay diagram generation
        triangles = voronoi.computeDelaunayTriangulation(seeds)
        for triangle in triangles:
            pt1   = seeds[triangle[0]]
            pt2   = seeds[triangle[1]]
            pt3   = seeds[triangle[2]]
            cmds  = [
                     ["M", [pt1.x, pt1.y]],
                     ["L", [pt2.x, pt2.y]],
                     ["L", [pt3.x, pt3.y]],
                     ["Z", []],
                    ]

            # Pick color at triangle midpoint
            if fill:
                xmid, ymid = (pt1 + pt2 + pt3)/3  # centroid of triangle
                if gtransform:
                    [xmid, ymid] = gtransform.apply_to_point([xmid, ymid])

                try:
                    (r, g, b) = image_array[int(ymid - left)][int(xmid - top)]
                except IndexError:
                    (r, g, b) = (127, 127, 127)  # if point somehow exceeds image size, use grey

                colorhex  = "#%02x%02x%02x" % (r, g, b)
                facestyle = {
                             "fill"            : colorhex,
                             "stroke"          : colorhex,
                             "stroke-width"    : strokewidth,
                             "stroke-linecap"  : "round",
                             "stroke-linejoin" : "round"
                            }

            # Draw triangle
            newpath = group.add(PathElement())
            newpath.set("d", str(Path(cmds)))
            newpath.style = facestyle

if __name__ == "__main__":
    LowPolyTrace().run()
