# Low Poly Trace

This extension will create a low poly diagram using Delaunay triangulation.

Selection must include at least one path, and in total, at least 3 distinct points. Objects, Text or Paths in Groups in selection will be ignored. Paths can be combined (but not grouped) for ease of selection. If grouped, then enter the group and select the paths before starting the extension.

Fill: Default fill and stroke color of each triangle is picked at the midpoint from the underlying drawing. If there are no objects at the location, the default page color or white will be used. If fill option is not selected, only black outlines will be drawn with no fill.

Range: Points nearby within the specified range will be ignored so that narrow or small triangles will not be created. Exact duplicates are automatically ignored. 

## Usage
- Requires **Inkscape 1.2+** (uses *unit_to_viewport* method) [tested on Windows 11]
- Requires *voronoi.py* in Inkscape's program extension folder, which comes with the regular Inkscape installation
- Save *lowpolytrace.inx* and *lowpolytrace.py* to your user extension folder, e.g. C:\Users\Username\AppData\Roaming\Inkscape\extensions
- Extension will appear in Extensions -> Generate from Path -> Low Poly Trace... menu in Inkscape
